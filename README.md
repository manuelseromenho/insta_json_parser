## Basic Instagram Parser
Author: Manuel Seromenho<br>Date: 29/05/2020

Based on the code of Arun Chaudhary
>https://github.com/its-arun/igdm-parser

Main Objective:
- Reads the messages.json exported from instagram, parses it, and exports the conversation into an html file.

Example:
    Run the python file with the participants names e.g:

    python3 -m insta_json_parser main_user

