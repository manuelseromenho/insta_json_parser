# Author: Manuel Seromenho
# Date: 29/05/2020
# Based on the code of Arun Chaudhary
# https://github.com/its-arun/igdm-parser

# -*- coding: utf-8 -*-

import json
import random
import sys

from .utils import get_colors, clamp


def create_and_write_html_file(messages_list, template_start, template_end):
    if len(messages_list) > 1:
        with open('chat.html', 'w') as file:
            file.write(template_start)
            file.write('\n')
        with open('chat.html', 'a') as f:
            for item in messages_list:
                f.write('%s\n' % item)
            f.write(template_end)
        print('Parsed '+str(len(messages_list))+' messages to chat.html')


def create_html(data, searched_participants):
    palete_colors = []

    random.seed(12)
    for i in range(0, 10):
        palete_colors.append(get_colors(palete_colors, pastel_factor=0.04))

    count = 1
    messages_list = []

    template_start = '<html><head><title>Instagram Export</title></head>' \
                     '<style>@import url(https://fonts.googleapis.com/css?family=Poppins:200,400);\
    :root{--green:#00bc84;--light-grey:#dce8e8;--dark-grey:#4a575b}*{box-sizing:border-box;margin:0;padding:0}\
    body{font-family:Poppins,sans-serif;font-size:14px;color:var(--dark-grey)}\
    img{height:400px;margin:0 1em;box-shadow:0 2px 5px rgba(0,0,0,.1);' \
    'align-self:flex-end}input[type=text]:focus{outline:0}'\
    'h3{font-size:22px;font-weight:300;padding:2rem 0 4rem 0;text-transform:uppercase}\
    h3 span{display:block;text-align:center;color:var(--green);font-size:16px;font-weight:400;\
    border-bottom:1px solid var(--light-grey)}\
    .wrapper{max-width:1000px;margin:0 auto;padding:0 1em;\
    display:flex;flex-direction:column;justify-content:center;align-items:center}\
    .chat-item{display:flex;align-items:center;margin-bottom:1em;overflow:hidden}\
    .chat-item_body{padding:1em 5em;border:1px solid var(--light-grey);\
    border-radius:40px;border-bottom-left-radius:0;box-shadow:0 1px 5px rgba(0,0,0,.1);\
    align-self:flex-end}.chat-item_body.alt{background-color:var(--light-grey);\
    border-bottom-left-radius:0}\
    .new-message{margin-top:4em;margin-bottom:2rem;\
    display:flex;border:1px solid var(--green);border-radius:20px;overflow:hidden}\
    .message-body{border:0;padding:1em 2rem}.message-button{background-color:var(--green);\
    border:none;padding:0 1em;color:var(--light-grey);cursor:pointer}</style>'\
                     '<body><div class="wrapper"><h3>Parsing Instagram Messages</h3><div class="chat-interface">'

    chat_figure = '<span class="chat-item">'
    chat_figcaption = '<span class="chat-item_body">'
    chat_figcaption_alt = '<span class="chat-item_body alt">'
    chat_fig_end = '</span></span>'

    message_html = ''
    for interaction in data:
        participants = interaction['participants']
        conversation = interaction['conversation']

        if any(part in searched_participants for part in participants):

            color_of_participant = {participants[i]: palete_colors[i] for (i, name) in enumerate(participants)}

            print('**********************************************************************')
            print('START NEW CONVERSATION************************************************')
            print('**********************************************************************')
            print('Conversa nr {}'.format(count))
            if count == 1:
                messages_list.append('<h1>CONVERSA nr {}</h1>'.format(count))
            else:
                messages_list.append(
                    '<br><br><br><br><hr><br><br><h1>CONVERSA nr {}</h1>'.format(count)
                )

            count += 1
            print(interaction['participants'])

            for message in conversation:
                # base_html_p = '{} , {} : {} <br>'
                time_str = message['created_at']
                sender = message['sender']
                time_str_slipt = time_str.split('T')
                created_at = ('{} {}').format(time_str_slipt[0], time_str_slipt[1][:8])
                if 'text' in message:
                    message_ = chat_figure + chat_figcaption + str(message['text']) + chat_fig_end
                    message_html = created_at + " " + sender + message_

                elif 'media' in message:
                    base_html = '{} , {} :'
                    if '.jpng' in message['media']:
                        image_html = '<img src="{}" width="200px"><br>'
                        message_html = (base_html+image_html).format(
                            created_at,
                            sender,
                            message['media']
                        )
                    elif '.mp4' in message['media']:
                        video_html = '<video width="200px"><source src="{}" type="video/mp4"></video>'
                        message_html = (base_html+video_html).format(
                            created_at,
                            sender,
                            message['media']
                        )
                elif 'story_share' in message:
                    base_html = '{} , {} : {} <br>'
                    message_html = base_html.format(
                        created_at,
                        sender,
                        message['story_share']
                    )
                elif 'profile_share_username' in message:
                    message_html = base_html.format(
                        created_at,
                        sender,
                        message['profile_share_username'],
                        message['profile_share_name']
                    )
                elif 'media_owner' in message:
                    base_html = '{} , {} : {} {} <br>'
                    if '.jpg' in message['media_share_url']:
                        image_html = '<img src="{}" width="200px"><br>'
                        message_html = (base_html+image_html).format(
                            created_at,
                            sender,
                            message['media_owner'],
                            message['media_share_caption'],
                            message['media_share_url'],
                        )
                    elif '.mp4' in message['media_share_url']:
                        video_html = '<video width="400"><source src="{}" type="video/mp4">VIDEO NOT FOUND!</video><br>'
                        message_html = (base_html+video_html).format(
                            created_at,
                            sender,
                            message['media_owner'],
                            message['media_share_caption'],
                            message['media_share_url'],
                        )

                the_color = tuple([i * 255 for i in color_of_participant[sender]])
                r = int(the_color[0])
                g = int(the_color[1])
                b = int(the_color[2])
                hex_the_color = "#{0:02x}{1:02x}{2:02x}".format(clamp(r), clamp(g), clamp(b))
                messages_list.append('<div style="color: {};">'.format(hex_the_color) + message_html + '</div>')


            print('**********************************************************************')
            print('End Conversation *****************************************************')
            print('**********************************************************************')
            print('\n\n\n')

    template_end = '</div></div></body>'

    return {'messages_list': messages_list, 'template_start': template_start, 'template_end': template_end}


def run():
    url = 'messages.json'
    try:
        with open(url, 'r') as read_file:
            data = json.load(read_file)

        searched_participants = sys.argv[1:]

        html_to_write = create_html(data, searched_participants)
        messages_list = html_to_write['messages_list']
        print("Creating and writing to html file...")
        create_and_write_html_file(messages_list,  html_to_write['template_start'], html_to_write['template_end'])
        print("Finished...check chat.html file")
    except (IOError, json.decoder.JSONDecodeError):
        print("messages.json not present in folder or bad file")

    
